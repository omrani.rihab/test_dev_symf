<?php

// src/Controller/Home.php
namespace App\Controller;

use App\Service\RssService;
use App\Service\ApiService;
use App\Service\ImageService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class Home extends AbstractController
{
    private $rssService;
    private $apiService;
    private $imageService;

    public function __construct(RssService $rssService, ApiService $apiService, ImageService $imageService, LoggerInterface $logger, HttpClientInterface $httpClient, ParameterBagInterface $params, CacheInterface $cache)
    {
        $this->rssService = $rssService;
        $this->apiService = $apiService;
        $this->imageService = $imageService;
        $this->logger = $logger;
        $this->httpClient = $httpClient;
        $this->params = $params;
        $this->cache = $cache;
    }

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        //$rssLinks = $this->rssService->getLinks('http://www.commitstrip.com/en/feed/');
                
        // Utilisation du cache pour récupérer les liens RSS
        $rssLinks = $this->cache->get('rss_links', function (ItemInterface $item) {
            $item->expiresAfter(3600); // Cache valide pendant 1 heure

            return $this->rssService->getLinks('http://www.commitstrip.com/en/feed/');
        });    
        
        $apiLinks = $this->apiService->getLinks('https://newsapi.org/v2/top-headlines?country=us&apiKey=c782db1cd730403f88a544b75dc2d7a0');
        $uniqueLinks = $this->imageService->filterAndRemoveDuplicates($rssLinks, $apiLinks);

        $images = [];
        foreach ($uniqueLinks as $link) {
            try {
                $images[] = $this->imageService->getImageFromUrl($link);
            } catch (\Exception $e) {
                $this->logger->error('Erreur lors de la récupération d\'une image: ' . $e->getMessage());
            }
        }

        return $this->render('default/index.html.twig', ['images' => $images]);
    }
}