<?php
// src/Service/ImageService.php
namespace App\Service;

class ImageService
{
    public function filterAndRemoveDuplicates($links1, $links2)
    {
        $filteredLinks = array_merge(array_diff($links1, $links2), array_diff($links2, $links1));
        return array_values(array_filter(array_unique($filteredLinks)));
    }

    public function getImageFromUrl($url)
    {
        $doc = new \DomDocument();
        @$doc->loadHTMLFile($url);
        $xpath = new \DomXpath($doc);

        $query = '//img/@src';

        if (strstr($url, "commitstrip.com")) {
            $query = '//img[contains(@class,"size-full")]/@src';
        }

        $xq = $xpath->query($query);

        return $xq[0]->value;
    }
}
