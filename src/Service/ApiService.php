<?php

// src/Service/ApiService.php
namespace App\Service;
use Psr\Log\LoggerInterface;

class ApiService
{

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    public function getLinks($apiUrl)
    {
        $links = [];

        try {
            $jsonData = file_get_contents($apiUrl);
            $data = json_decode($jsonData);

            for ($i = 0; $i < count($data->articles); $i++) {
                $urlToImage = $data->articles[$i]->urlToImage;
                if (!empty($urlToImage)) {
                    $links[] = $data->articles[$i]->url;
                }
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $links;

    }
}
