<?php
// src/Service/RssService.php
namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;

class RssService
{
    private $httpClient;
    private $params;

    private $logger;

    public function __construct(HttpClientInterface $httpClient, ParameterBagInterface $params, LoggerInterface $logger)
    {
        $this->httpClient = $httpClient;
        $this->params = $params;
        $this->logger = $logger;
    }
    public function getLinks($rssUrl)
    {
        $links = [];

        try {
            $xmlData = $this->httpClient->request('GET', $rssUrl)->getContent();
            $xmlData = file_get_contents($rssUrl);
            $xml = simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA);
            $channel = $xml->channel;

            for ($i = 1; $i < count($channel->item); $i++) {
                $link = (string)$channel->item[$i]->link;
                $links[$i] = $link;
            }

            // Filter and remove invalid links
            $links = array_filter($links, function ($link) {
                return preg_match('/\.(jpg|jpeg|png|gif)$/i', $link);
            });
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $links;
    }

    public function getLinksAsync($rssUrl)
    {
        return $this->httpClient->requestAsync('GET', $rssUrl)->then(function ($response) {
            $xmlData = $response->getContent();
            $xml = simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA);
            $channel = $xml->channel;

            $links = [];
            for ($i = 1; $i < count($channel->item); $i++) {
                $link = (string)$channel->item[$i]->link;
                $links[$i] = $link;
            }

            // Filter and remove invalid links
            $links = array_filter($links, function ($link) {
                return preg_match('/\.(jpg|jpeg|png|gif)$/i', $link);
            });

            return $links;
        });
    }
}