test-dev
========

Un stagiaire à créer le code contenu dans le fichier src/Controller/Home.php

Celui permet de récupérer des urls via un flux RSS ou un appel à l’API NewsApi. 
Celles ci sont filtrées (si contient une image) et dé doublonnées. 
Enfin, il faut récupérer une image sur chacune de ces pages.

Le lead dev n'est pas très satisfait du résultat, il va falloir améliorer le code.

Pratique : 
1. Revoir complètement la conception du code (découper le code afin de pouvoir ajouter de nouveaux flux simplement) 

Questions théoriques : 
1. Que mettriez-vous en place afin d'améliorer les temps de réponses du script
2. Comment aborderiez-vous le fait de rendre scalable le script (plusieurs milliers de sources et images)



 Que mettriez-vous en place afin d'améliorer les temps de réponses du script


1 Parallélisation des Requêtes :
• Utilisez des requêtes parallèles pour récupérer les liens à partir du flux RSS et de l'API NewsApi. Cela peut être réalisé à l'aide de tâches asynchrones ou de processus parallèles.

2	Caching :
•	Pour le caching, utilisez le composant Cache de Symfony. Vous pouvez mettre en cache les résultats des requêtes RSS et API pour éviter des requêtes redondantes.

3	Optimisation des Boucles :
•	Optimisez les boucles pour réduire le temps d'exécution, par exemple en sortant les conditions constantes du corps de la boucle.


Comment aborderiez-vous le fait de rendre scalable le script (plusieurs milliers de sources et images)

1 	Traitement Asynchrone :
•	Si vous prévoyez de gérer plusieurs milliers de sources, envisagez d'utiliser des tâches asynchrones ou des queues de message pour traiter les tâches en arrière-plan et éviter de bloquer le script principal.


2 Optimisation des Requêtes d'Image :
•	Si la récupération des images prend beaucoup de temps, envisagez de paralléliser également ce processus. Vous pourriez utiliser des bibliothèques comme Guzzle pour effectuer des requêtes HTTP de manière asynchrone.

3	Éviter les Structures de Données Redondantes :
•	Évitez la création de structures de données redondantes lors de la suppression des doublons. Il pourrait y avoir une meilleure approche pour cette logique.

4	Gestion des Erreurs et Retries :
•	Ajoutez une gestion appropriée des erreurs avec des mécanismes de retry, surtout lors de l'accès à des sources externes comme les flux RSS et l'API NewsApi.

